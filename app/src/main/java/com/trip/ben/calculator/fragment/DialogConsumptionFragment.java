package com.trip.ben.calculator.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.trip.ben.calculator.R;
import com.trip.ben.calculator.activity.UpdateSettingsActivity;
import com.trip.ben.calculator.activity.UserSettingsActivity;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DialogConsumptionFragment extends DialogFragment {

    private ListView listView;
    private String[] arrayOfConsumptionUnits;
    private Context context;
    private boolean disabledKm;
    private boolean disabledMiles;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.consumption_units);
        View rootView = inflater.inflate(R.layout.fragment_dialog_consumption, container, false);
        arrayOfConsumptionUnits = getResources().getStringArray(R.array.units_consumption);
        listView = rootView.findViewById(R.id.f_consumption_list);

        if (context != null) {
            if (context instanceof UserSettingsActivity)
                disabledKm = ((UserSettingsActivity) getActivity()).isDisabledKm();
            else if (context instanceof UpdateSettingsActivity)
                disabledKm = ((UpdateSettingsActivity) getActivity()).isDisabledKm();
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, arrayOfConsumptionUnits) {
            @Override
            public boolean isEnabled(int position) {
                if (disabledKm) {
                    return position != 0;
                } else {
                    return position == 0;
                }
            }

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                if (disabledKm && position == 0) {
                    view.setBackgroundColor(Color.GRAY);
                } else if (disabledMiles && position != 0) {
                    view.setBackgroundColor(Color.GRAY);
                } else {
                    view.setBackgroundColor(Color.WHITE);
                }
                return view;
            }
        };
        arrayAdapter.notifyDataSetChanged();
        listView.setAdapter(arrayAdapter);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView.setOnItemClickListener((parent, view1, position, id) -> {
            if (context != null) {
                if (context instanceof UserSettingsActivity) {
                    ((UserSettingsActivity) getActivity())
                            .setUnitsConsumptionInCardView(arrayOfConsumptionUnits[position]);
                    this.dismiss();
                } else if (context instanceof UpdateSettingsActivity) {
                    ((UpdateSettingsActivity) getActivity())
                            .setUnitsConsumptionInCardView(arrayOfConsumptionUnits[position]);
                    this.dismiss();
                }
            } else
                this.dismiss();
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}