package com.trip.ben.calculator.domain;

import com.trip.ben.calculator.R;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Benjamin on 2017-11-02.
 */
@Getter
@Setter
public class Map extends FrontPageCard {
    private String name;
    private String description;
    private String header;
    private int iconId = R.drawable.ic_maps;

    private static final Map ourInstance = new Map();

    public static Map getInstance() {
        return ourInstance;
    }

    private Map() {
    }
}
