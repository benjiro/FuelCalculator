package com.trip.ben.calculator.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mynameismidori.currencypicker.CurrencyPicker;
import com.mynameismidori.currencypicker.CurrencyPickerListener;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.activity.FrontActivity;
import com.trip.ben.calculator.activity.MapsActivity;
import com.trip.ben.calculator.activity.SavedSettingsActivity;
import com.trip.ben.calculator.controller.DatabaseController;
import com.trip.ben.calculator.database.UserDatabase;
import com.trip.ben.calculator.domain.ApplicationCurrency;
import com.trip.ben.calculator.domain.FrontPageCard;

import java.util.List;

/**
 * Created by Benjamin on 2017-11-02.
 */

public class MyAdapter extends RecyclerView.Adapter {

    private UserDatabase userDatabase;
    private List<FrontPageCard> mCards;
    private RecyclerView mRecyclerView;
    private Context context;

    public MyAdapter(List<FrontPageCard> pCards, RecyclerView pRecyclerView, Context context) {
        mCards = pCards;
        mRecyclerView = pRecyclerView;
        this.context = context;
        userDatabase = new UserDatabase(context);
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mContent;
        public TextView mHeader;
        public ImageView mIcon;

        public MyViewHolder(View pItem) {
            super(pItem);
            mHeader = pItem.findViewById(R.id.header_of_card);
            mTitle = pItem.findViewById(R.id.card_name_text);
            mContent = pItem.findViewById(R.id.card_info_text);
            mIcon = pItem.findViewById(R.id.card_icon);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.front_layout, viewGroup, false);

        view.setOnClickListener((View v) -> {
            int position = mRecyclerView.getChildAdapterPosition(v);
            startNewActivity(position, context);
        });

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
        FrontPageCard frontPageCard = mCards.get(i);
        ((MyViewHolder) viewHolder).mTitle.setText(frontPageCard.getName());
        ((MyViewHolder) viewHolder).mContent.setText(frontPageCard.getDescription());
        ((MyViewHolder) viewHolder).mHeader.setText(frontPageCard.getHeader());
        ((MyViewHolder) viewHolder).mIcon.setBackgroundResource(frontPageCard.getIconId());
    }

    @Override
    public int getItemCount() {
        return mCards.size();
    }

    private void startNewActivity(int position, Context context) {
        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent(context, SavedSettingsActivity.class);
                context.startActivity(intent);
                break;
            case 1:
                intent = new Intent(context, SavedSettingsActivity.class);
                context.startActivity(intent);
                break;
            case 2:
                intent = new Intent(context, MapsActivity.class);
                context.startActivity(intent);
                break;
            case 3:
                CurrencyPicker picker = CurrencyPicker.newInstance(context.getString(R.string.select_currency));
                picker.setListener((s, s1, s2, i) -> {
                    ApplicationCurrency applicationCurrency = new ApplicationCurrency();
                    applicationCurrency.setName(s);
                    applicationCurrency.setCurrency(s2);
                    applicationCurrency.setId(0);
                    userDatabase.updateUserCurrency(applicationCurrency);
                    picker.dismiss();
                    mCards.remove(3);
                    mCards.add(new DatabaseController(context).getChosenCurrency());
                    notifyDataSetChanged();
                });
                picker.show(((FrontActivity) context).getSupportFragmentManager(), "CURRENCY_PICKER");
            default:
                break;
        }
    }
}
