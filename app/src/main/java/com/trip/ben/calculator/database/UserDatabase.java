package com.trip.ben.calculator.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.trip.ben.calculator.domain.ApplicationCurrency;
import com.trip.ben.calculator.domain.Settings;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Ben on 2017-04-12.
 */

public class UserDatabase extends SQLiteOpenHelper {


    public UserDatabase(Context context) {
        super(context, "saved_settings12.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table usersettings(" +
                "nr integer primary key autoincrement," +
                "car_name text," + "avg_fuel float," +
                "units_dist text," + "units_consumption text," + "checked boolean);" + "");

        db.execSQL("create table user_currency(" + "id integer primary key autoincrement," +
                "currency_name text," + "country_name text);" + "");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void setUserCurrency(ApplicationCurrency applicationCurrency) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("id", applicationCurrency.getId());
        cv.put("currency_name", applicationCurrency.getCurrency());
        cv.put("country_name", applicationCurrency.getName());
        db.insertOrThrow("user_currency", null, cv);
    }

    public ApplicationCurrency getCurrencyById(int id) {
        ApplicationCurrency applicationCurrency = new ApplicationCurrency();
        SQLiteDatabase db = getReadableDatabase();
        String[] cols = {"id", "currency_name", "country_name"};
        String args[] = {id + ""};
        Cursor c = db.query("user_currency", cols, " id+?", args, null, null, null, null);
        if (c != null  && c.moveToFirst()) {
            c.moveToFirst();
            applicationCurrency.setId(c.getInt(0));
            applicationCurrency.setCurrency(c.getString(1));
            applicationCurrency.setName(c.getString(2));
        }
        return applicationCurrency;
    }

    public void updateUserCurrency(ApplicationCurrency applicationCurrency) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("currency_name", applicationCurrency.getCurrency());
        cv.put("country_name", applicationCurrency.getName());
        String args[] = {applicationCurrency.getId() + ""};
        db.update("user_currency", cv, "id=?", args);
    }


    public void addUserSettings(Settings settings) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("car_name", settings.getCarName());
        cv.put("avg_fuel", settings.getConsumption());
        cv.put("units_dist", settings.getUnitsDistance());
        cv.put("units_consumption", settings.getUnitsConsumption());
        cv.put("checked", settings.isChecked());
        db.insertOrThrow("usersettings", null, cv);
    }

    public void deleteUserSettings(int id) {
        SQLiteDatabase db = getWritableDatabase();
        String[] arguments = {"" + id};
        db.delete("usersettings", "nr=?", arguments);
    }

    public void updateUserSettings(Settings settings) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("car_name", settings.getCarName());
        cv.put("avg_fuel", settings.getConsumption());
        cv.put("units_dist", settings.getUnitsDistance());
        cv.put("units_consumption", settings.getUnitsConsumption());
        cv.put("checked", settings.isChecked());
        String args[] = {settings.getNr() + ""};
        db.update("usersettings", cv, "nr=?", args);
    }

    public Settings readSettings(int nr) {
        Settings settings = new Settings();
        SQLiteDatabase db = getReadableDatabase();
        String[] cols = {"nr", "car_name", "avg_fuel", "units_dist", "units_consumption", "checked"};
        String args[] = {nr + ""};
        Cursor c = db.query("usersettings", cols, " nr+?", args, null, null, null, null);
        if (c != null) {
            c.moveToFirst();
            settings.setNr(c.getInt(0));
            settings.setCarName(c.getString(1));
            settings.setConsumption(c.getFloat(2));
            settings.setUnitsDistance(c.getString(3));
            settings.setUnitsConsumption(c.getString(4));
            settings.setChecked(c.getInt(5) > 0);
        }
        return settings;
    }

    public List<ApplicationCurrency> getAllCurrencies() {
        List<ApplicationCurrency> currencyList = new LinkedList<>();
        String[] columns = {"id", "currency_name", "country_name"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("user_currency", columns, null, null, null, null, null);
        while (cursor.moveToNext()) {
            ApplicationCurrency applicationCurrency = new ApplicationCurrency();
            applicationCurrency.setId(cursor.getInt(0));
            applicationCurrency.setCurrency(cursor.getString(1));
            applicationCurrency.setName(cursor.getString(2));
            currencyList.add(applicationCurrency);
        }
        return currencyList;
    }

    public List<Settings> getAll() {
        List<Settings> settingsList = new LinkedList<>();
        String[] columns = {"nr", "car_name", "avg_fuel", "units_dist", "units_consumption", "checked"};
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query("usersettings", columns, null, null, null, null, null);
        while (cursor.moveToNext()) {
            Settings settings = new Settings();
            settings.setNr(cursor.getInt(0));
            settings.setCarName(cursor.getString(1));
            settings.setConsumption(cursor.getFloat(2));
            settings.setUnitsDistance(cursor.getString(3));
            settings.setUnitsConsumption(cursor.getString(4));
            settings.setChecked(cursor.getInt(5) > 0);
            settingsList.add(settings);
        }
        return settingsList;
    }
}
