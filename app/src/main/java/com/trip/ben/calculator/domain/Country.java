package com.trip.ben.calculator.domain;

import com.trip.ben.calculator.R;
import com.trip.ben.calculator.activity.FrontActivity;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Benjamin on 2017-11-08.
 */

@Getter
@Setter
public class Country extends FrontPageCard {
    private String name;
    private String description;
    private String header;
    private int iconId = R.drawable.ic_monetization;
    private static final Country ourInstance = new Country();

    public static Country getInstance() {
        return ourInstance;
    }

    private Country() {
    }
}
