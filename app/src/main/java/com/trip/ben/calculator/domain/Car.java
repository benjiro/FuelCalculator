package com.trip.ben.calculator.domain;

import com.trip.ben.calculator.R;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Benjamin on 2017-11-02.
 */

@Getter
@Setter
public class Car extends FrontPageCard{
    private String name;
    private String description;
    private String header;
    private int iconId = R.drawable.ic_car;
    private static final Car ourInstance = new Car();

    public static Car getInstance() {
        return ourInstance;
    }

    private Car() {
    }
}
