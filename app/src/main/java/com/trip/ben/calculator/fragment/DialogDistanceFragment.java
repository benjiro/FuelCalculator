package com.trip.ben.calculator.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.trip.ben.calculator.R;
import com.trip.ben.calculator.activity.UpdateSettingsActivity;
import com.trip.ben.calculator.activity.UserSettingsActivity;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class DialogDistanceFragment extends DialogFragment {

    private ListView listView;
    private String[] arrayOfDistanceUnits;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.distance_units);
        return inflater.inflate(R.layout.fragment_dialog_distance, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listView = view.findViewById(R.id.f_distance_list);
        arrayOfDistanceUnits = getResources().getStringArray(R.array.units_distance);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_activated_1, arrayOfDistanceUnits);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            if (context != null) {
                if (context instanceof UserSettingsActivity) {
                    ((UserSettingsActivity) getActivity()).setUnitsDistanceInCardView(arrayOfDistanceUnits[position]);
                    this.dismiss();
                } else if (context instanceof UpdateSettingsActivity) {
                    ((UpdateSettingsActivity) getActivity()).setUnitsDistanceInCardView(arrayOfDistanceUnits[position]);
                    this.dismiss();
                }
            } else
                this.dismiss();
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }
}