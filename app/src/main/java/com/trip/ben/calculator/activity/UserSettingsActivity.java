package com.trip.ben.calculator.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.database.UserDatabase;
import com.trip.ben.calculator.domain.Settings;
import com.trip.ben.calculator.fragment.DialogConsumptionFragment;
import com.trip.ben.calculator.fragment.DialogDistanceFragment;

import java.util.List;

import lombok.Getter;
import mehdi.sakout.fancybuttons.FancyButton;
import solid.stream.Stream;

public class UserSettingsActivity extends AppCompatActivity {

    private static final String DISTANCE_TAG = "DISTANCE";
    private static final String CONSUMPTION_TAG = "CONSUMPTION";
    private AdView mAdView;
    private UserDatabase userDatabase;
    private List<Settings> list;
    private Context context;
    @Getter
    private boolean disabledMiles;
    @Getter
    private boolean disabledKm;
    private FancyButton btnAdd;
    private CardView cvDistance;
    private CardView cvConsumption;
    private TextView tvChosenDistanceUnits;
    private TextView tvChosenConsumptionUnits;
    private EditText carName;
    private EditText averageConsumption;
    private String[] arrayOfConsumptionUnits;
    private String[] arrayOfDistanceUnits;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        Toolbar topToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(topToolBar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        carName = findViewById(R.id.editText_car);
        averageConsumption = findViewById(R.id.editText_avg_consumption);

        cvDistance = findViewById(R.id.cv_user_settings_distance);
        cvConsumption = findViewById(R.id.cv_user_settings_consumption);
        tvChosenDistanceUnits = findViewById(R.id.tv_chosen_distance);
        tvChosenConsumptionUnits = findViewById(R.id.tv_chosen_consumption);

        arrayOfConsumptionUnits = getResources().getStringArray(R.array.units_consumption);
        arrayOfDistanceUnits = getResources().getStringArray(R.array.units_distance);
        btnAdd = findViewById(R.id.btn_add_new_settings);

        disableSomeSortOfUnits(tvChosenDistanceUnits.getText().toString());

        cvDistance.setOnClickListener(v -> {
            DialogDistanceFragment dialogDistanceFragment = new DialogDistanceFragment();
            dialogDistanceFragment.show(getSupportFragmentManager(), DISTANCE_TAG);
        });

        cvConsumption.setOnClickListener(v -> {
            if (!disabledMiles) {
                DialogConsumptionFragment dialogConsumptionFragment = new DialogConsumptionFragment();
                dialogConsumptionFragment.show(getSupportFragmentManager(), CONSUMPTION_TAG);
            }
        });
        tvChosenConsumptionUnits.setText(arrayOfConsumptionUnits[0]);


        context = getApplicationContext();
        userDatabase = new UserDatabase(context);

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        btnAdd.setOnClickListener(v -> {
            String[] inputTextsOnPage = {carName.getText().toString(), averageConsumption.getText().toString()};
            if (Stream.stream(inputTextsOnPage).any(s -> s.equals(""))) {
                Toast.makeText(UserSettingsActivity.this, R.string.blank_fields, Toast.LENGTH_LONG).show();
            } else {
                list = userDatabase.getAll();
                for (Settings s : list) {
                    s.setChecked(false);
                    userDatabase.updateUserSettings(s);
                }

                Settings settings = new Settings();
                settings.setCarName(carName.getText().toString());
                settings.setConsumption(Float.parseFloat(averageConsumption.getText().toString()));
                settings.setUnitsConsumption(tvChosenConsumptionUnits.getText().toString());
                settings.setUnitsDistance(tvChosenDistanceUnits.getText().toString());
                settings.setChecked(true);

                userDatabase.addUserSettings(settings);
                Toast.makeText(UserSettingsActivity.this, R.string.toast_settings_saved, Toast.LENGTH_SHORT).show();
                finish();
                SavedSettingsActivity.activity.finish();
                startActivity(new Intent(UserSettingsActivity.this, SavedSettingsActivity.class));
            }
        });
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public void setUnitsDistanceInCardView(String text) {
        tvChosenDistanceUnits.setText(text);
        disableSomeSortOfUnits(text);
    }

    public void setUnitsConsumptionInCardView(String text) {
        tvChosenConsumptionUnits.setText(text);
    }

    public void disableSomeSortOfUnits(String text) {
        if (text.equals(getResources().getStringArray(R.array.units_distance)[0])) {
            tvChosenConsumptionUnits.setText(arrayOfConsumptionUnits[0]);
            disabledMiles = true;
            disabledKm = false;
        } else {
            if (tvChosenConsumptionUnits.getText().toString().equals(arrayOfConsumptionUnits[0]))
                tvChosenConsumptionUnits.setText(arrayOfConsumptionUnits[1]);
            disabledMiles = false;
            disabledKm = true;
        }
    }
}
