package com.trip.ben.calculator.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.database.UserDatabase;
import com.trip.ben.calculator.domain.Country;
import com.trip.ben.calculator.domain.Settings;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.List;

import mehdi.sakout.fancybuttons.FancyButton;
import solid.stream.Stream;

public class CalculatorActivity extends AppCompatActivity {

    private AdView mAdView;
    private List<Settings> list;
    private UserDatabase userDatabase;
    private FancyButton btnCount;
    private EditText etDistance;
    private EditText etPersons;
    private EditText etPrice;
    private EditText etConsumption;
    private Country country = Country.getInstance();
    private Settings settings;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
        initializeToolbar();
        initializeView();

        Bundle extras = getIntent().getExtras();
        userDatabase = new UserDatabase(this);
        list = userDatabase.getAll();
        for (Settings s : list) {
            if (s.isChecked()) {
                settings = s;
                etConsumption.setText(Float.toString(s.getConsumption()));
            }
        }
        setDistanceInEditText(extras);
        setHintsUnits();

        btnCount.setOnClickListener(v -> {
            String result = countPricePerPerson(etDistance.getText().toString(),
                    etConsumption.getText().toString(), etPrice.getText().toString(),
                    etPersons.getText().toString());
            StringBuilder shownStringAsResult = new StringBuilder(result);
            if (!country.getDescription()
                    .equals(getResources().getString(R.string.not_chosen_currency_symbol))) {
                shownStringAsResult.append(" ").append(country.getDescription());
            }
            if (!result.equals("")) {
                new LovelyStandardDialog(this)
                        .setTopColorRes(R.color.colorPrimary)
                        .setIcon(R.drawable.ic_monetization)
                        .setTitle(R.string.price_per_person)
                        .setMessage(shownStringAsResult)
                        .setPositiveButton("OK", v1 -> {
                        })
                        .show();

            } else {
                Toast.makeText(CalculatorActivity.this, R.string.blank_fields, Toast.LENGTH_LONG).show();
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    private String countPricePerPerson(String distance, String consumption, String price, String numberOfPeople) {
        String[] arrayOfData = {distance, consumption, price, numberOfPeople};
        if (Stream.stream(arrayOfData).any(s -> s.equals(""))) {
            return "";
        } else if (settings != null && settings.getUnitsConsumption().equals("MPG UK")) {
            return String.format("%.2f", Double.parseDouble(etDistance.getText().toString()) / 0.621371192
                    / 100 * 282.48 / Double.parseDouble(etConsumption.getText().toString())
                    * Double.parseDouble(etPrice.getText().toString())
                    / Double.parseDouble(etPersons.getText().toString()));
        } else if (settings != null && settings.getUnitsConsumption().equals("MPG US")) {
            return String.format("%.2f", Double.parseDouble(etDistance.getText().toString()) / 0.621371192
                    / 100 * 235.21 / Double.parseDouble(etConsumption.getText().toString())
                    * Double.parseDouble(etPrice.getText().toString()) * 0.264172052
                    / Double.parseDouble(etPersons.getText().toString()));
        } else {
            return String.format("%.2f", Double.parseDouble(etDistance.getText().toString())
                    / 100 * Double.parseDouble(etConsumption.getText().toString())
                    * Double.parseDouble(etPrice.getText().toString())
                    / Double.parseDouble(etPersons.getText().toString()));
        }
    }

    private void setHintsUnits() {
        String distance = getString(R.string.distance_pure);
        String avgFuelConsumption = getString(R.string.avg_fuel);
        String numberOfPeople = getString(R.string.number_of_people);
        if (settings != null) {
            StringBuilder sbDistance = new StringBuilder(distance)
                    .append(" [")
                    .append(settings.getUnitsDistance())
                    .append("]");
            StringBuilder sbFuelConsumption = new StringBuilder(avgFuelConsumption)
                    .append(" [")
                    .append(settings.getUnitsConsumption())
                    .append("]");
            etDistance.setHint(sbDistance);
            etConsumption.setHint(sbFuelConsumption);
            etPersons.setHint(numberOfPeople);
        } else {
            StringBuilder sbDistance = new StringBuilder(distance)
                    .append(" [")
                    .append(getString(R.string.km))
                    .append("]");
            StringBuilder sbFuelConsumption = new StringBuilder(avgFuelConsumption)
                    .append(" [")
                    .append(getString(R.string.l_100km))
                    .append("]");
            etDistance.setHint(sbDistance);
            etConsumption.setHint(sbFuelConsumption);
            etPersons.setHint(numberOfPeople);
        }
        if (!country.getDescription()
                .equals(getResources().getString(R.string.not_chosen_currency_symbol))) {
            String fuelPrice = getString(R.string.fuel_price);
            if (settings != null) {
                if (settings.getUnitsConsumption().equals("MPG US")) {
                    StringBuilder sbFuelPrice = new StringBuilder(fuelPrice)
                            .append(" [")
                            .append(country.getDescription())
                            .append("/")
                            .append("gal]");
                    etPrice.setHint(sbFuelPrice);
                } else {
                    StringBuilder sbFuelPrice = new StringBuilder(fuelPrice)
                            .append(" [")
                            .append(country.getDescription())
                            .append("/")
                            .append("l]");
                    etPrice.setHint(sbFuelPrice);
                }
            } else {
                StringBuilder sbFuelPrice = new StringBuilder(fuelPrice)
                        .append(" [")
                        .append(country.getDescription())
                        .append("/")
                        .append("l]");
                etPrice.setHint(sbFuelPrice);
            }
        } else {
            String fuelPrice = getString(R.string.fuel_price);
            etPrice.setHint(fuelPrice);
        }
    }

    private void initializeView() {
        btnCount = findViewById(R.id.btn_count);
        etDistance = findViewById(R.id.et_distance);
        etPersons = findViewById(R.id.et_persons);
        etPrice = findViewById(R.id.et_fuel_price);
        etConsumption = findViewById(R.id.et_avg_consumption);
    }

    private void initializeToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    private void setDistanceInEditText(Bundle extras) {
        if (extras != null) {
            float distance;
            if (settings != null) {
                if (settings.getUnitsDistance().equals("miles")) {
                    distance = extras.getFloat("etDistance") * 0.621371192f / 1000;
                } else {
                    distance = extras.getFloat("etDistance") / 1000;
                }
            } else {
                distance = extras.getFloat("etDistance") / 1000;
            }
            float roundDistance = (float) Math.round(distance * 100f) / 100f;
            etDistance.setText(String.valueOf(roundDistance));
        }
    }
}