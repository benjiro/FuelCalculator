package com.trip.ben.calculator.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Ben on 2017-04-13.
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Settings {
    private int nr;
    private String carName;
    private Float consumption;
    private String unitsDistance;
    private String unitsConsumption;
    private boolean checked;
}
