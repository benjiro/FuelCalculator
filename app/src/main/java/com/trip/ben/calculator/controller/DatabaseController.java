package com.trip.ben.calculator.controller;

import android.content.Context;

import com.mynameismidori.currencypicker.ExtendedCurrency;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.database.UserDatabase;
import com.trip.ben.calculator.domain.ApplicationCurrency;
import com.trip.ben.calculator.domain.Car;
import com.trip.ben.calculator.domain.Country;
import com.trip.ben.calculator.domain.FrontPageCard;
import com.trip.ben.calculator.domain.Map;
import com.trip.ben.calculator.domain.Settings;
import com.trip.ben.calculator.domain.Units;

import java.util.ArrayList;
import java.util.List;

import solid.stream.Stream;

/**
 * Created by Benjamin on 2017-11-02.
 */

public class DatabaseController {

    private Context context;
    private FrontPageCard chosenCar = Car.getInstance();
    private FrontPageCard chosenUnits = Units.getInstance();
    private FrontPageCard mapInfo = Map.getInstance();
    private FrontPageCard chosenCurrency = Country.getInstance();

    private UserDatabase userDatabase;

    public DatabaseController(Context context) {
        this.context = context;
        userDatabase = new UserDatabase(context);
    }

    public FrontPageCard getChosenCar() {
        Settings chosenCarFromSettings = Stream.stream(userDatabase.getAll())
                .filter(Settings::isChecked)
                .first()
                .orNull();
        String carName = chosenCarFromSettings == null ? context.getString(R.string.you_havent_set_up_car)
                : chosenCarFromSettings.getCarName();
        String carDescription = chosenCarFromSettings == null ? context.getString(R.string.not_set)
                : chosenCarFromSettings.getConsumption().toString();
        chosenCar.setName(carName);
        chosenCar.setDescription(context.getString(R.string.fuel_consumption_semi_colon) + carDescription);
        return chosenCar;
    }

    public FrontPageCard getChosenUnits() {
        Settings chosenUnitsFromSettings = Stream.stream(userDatabase.getAll())
                .filter(Settings::isChecked)
                .first()
                .orNull();
        String unitsConsumption = chosenUnitsFromSettings == null ? context.getString(R.string.you_havent_set_settings)
                : chosenUnitsFromSettings.getUnitsConsumption();
        String unitsDistance = chosenUnitsFromSettings == null ? context.getString(R.string.not_set)
                : chosenUnitsFromSettings.getUnitsDistance();
        chosenUnits.setName(context.getString(R.string.distance_units_colon) + unitsDistance);
        chosenUnits.setDescription(context.getString(R.string.consumption_units_colon) + unitsConsumption);
        return chosenUnits;
    }

    public FrontPageCard getChosenCurrency() {
        List<ApplicationCurrency> currencyList = userDatabase.getAllCurrencies();
        if (currencyList.size() == 0) {
            ApplicationCurrency applicationCurrency =
                    new ApplicationCurrency(chosenCurrency.getName(), chosenCurrency.getDescription(), 0);
            userDatabase.setUserCurrency(applicationCurrency);
            return chosenCurrency;
        } else if (currencyList.get(0).getName().equals(context.getString(R.string.info_you_havent_chosen_currency))) {
            return chosenCurrency;
        } else {
            ApplicationCurrency applicationCurrency = currencyList.get(0);
            ExtendedCurrency currency = ExtendedCurrency.getCurrencyByName(applicationCurrency.getName());
            chosenCurrency.setName(applicationCurrency.getName());
            chosenCurrency.setDescription(applicationCurrency.getCurrency());
            chosenCurrency.setIconId(currency.getFlag());
            return chosenCurrency;
        }
    }

    public List<FrontPageCard> getListOfFrontPageCards() {
        List<FrontPageCard> frontPageCardList = new ArrayList<>();
        frontPageCardList.add(getChosenCar());
        frontPageCardList.add(getChosenUnits());
        frontPageCardList.add(mapInfo);
        frontPageCardList.add(getChosenCurrency());
        return frontPageCardList;
    }

    public int getIdOfChosenCar() {
        Settings settings = Stream.stream(userDatabase.getAll())
                .filter(Settings::isChecked)
                .first()
                .orNull();
        if (settings == null) {
            return -1;
        } else {
            return settings.getNr();
        }
    }
}
