package com.trip.ben.calculator.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Benjamin on 2017-11-08.
 */

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationCurrency {
    private String name;
    private String currency;
    private int id;
}
