package com.trip.ben.calculator.domain;

import com.trip.ben.calculator.R;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Benjamin on 2017-11-02.
 */

@Setter
@Getter
public class Units extends FrontPageCard {
    private String name;
    private String description;
    private String header;
    private int iconId = R.drawable.ic_settings;

    private static final Units ourInstance = new Units();

    public static Units getInstance() {
        return ourInstance;
    }

    private Units() {
    }
}
