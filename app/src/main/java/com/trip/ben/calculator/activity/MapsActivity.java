package com.trip.ben.calculator.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.database.UserDatabase;
import com.trip.ben.calculator.domain.Settings;
import com.trip.ben.calculator.tools.DataParser;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";
    private ProgressDialog progressDialog;
    private GoogleMap mMap;
    private Context context;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    private Settings settings;
    private LovelyStandardDialog dialogShowDistance;
    private PolylineOptions lineOptions;
    private Polyline routePolyline;
    private LatLng startLocation;
    private LatLng destinationLocation;
    private MarkerOptions startMarkerOptions = new MarkerOptions();
    private MarkerOptions destinationMarkerOptions = new MarkerOptions();
    private Marker startMarker;
    private Marker destinationMarker;
    private boolean clickGreen;
    private boolean clickRed;
    private boolean overLimitRequest;
    @Setter
    @Getter
    private float distance = 0f;
    private String distanceUnits = "km";
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        context = getApplicationContext();

        initializeToolbar();

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        createDialogWithDistance();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton fabStart = findViewById(R.id.fab_start);
        FloatingActionButton fabDestination = findViewById(R.id.fab_destination);

        fabStart.setOnClickListener(v -> {
            new LovelyTextInputDialog(this)
                    .setTopColorRes(R.color.colorGreen)
                    .setTitle(R.string.set_up_starting_point)
                    .setMessage(R.string.start)
                    .setIcon(R.drawable.ic_add_location_white_24dp)
                    .setInputFilter(R.string.blank_fields, text -> !text.equals(""))
                    .setConfirmButton(android.R.string.ok, text -> {
                        setLocationFlags(false, false);
                        startLocation = getLocationFromAddress(context, text);
                        setStartMarkerOnMap();
                        executeDrawingRoute();
                    })
                    .setNegativeButton(getString(R.string.click_on_map_to_set_point), v1 -> setLocationFlags(true, false))
                    .show();
        });

        fabDestination.setOnClickListener(v -> {
            new LovelyTextInputDialog(this)
                    .setTopColorRes(R.color.colorRed)
                    .setTitle(R.string.set_up_destination_point)
                    .setMessage(R.string.destination)
                    .setIcon(R.drawable.ic_add_location_white_24dp)
                    .setInputFilter(R.string.blank_fields, text -> !text.equals(""))
                    .setConfirmButton(android.R.string.ok, text -> {
                        setLocationFlags(false, false);
                        destinationLocation = getLocationFromAddress(context, text);
                        setDestinationMarkerOnMap();
                        executeDrawingRoute();
                    })
                    .setNegativeButton(getString(R.string.click_on_map_to_set_point), v1 -> setLocationFlags(false, true))
                    .show();
        });

        UserDatabase userDatabase = new UserDatabase(this);
        List<Settings> list = userDatabase.getAll();
        settings = new Settings();
        for (Settings settings : list) {
            if (settings.isChecked()) {
                this.settings = settings;
                distanceUnits = settings.getUnitsDistance();
            }
        }
        Toast.makeText(this, R.string.toast_map_init, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void setStartMarkerOnMap() {
        if (startMarker != null) startMarker.remove();
        startMarkerOptions.position(startLocation);
        startMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        startMarker = mMap.addMarker(startMarkerOptions);
    }

    public void setDestinationMarkerOnMap() {
        if (destinationMarker != null) destinationMarker.remove();
        destinationMarkerOptions.position(destinationLocation);
        destinationMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        destinationMarker = mMap.addMarker(destinationMarkerOptions);
    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 1);
            if (address == null) {
                Log.d("LocationInMethod", "Null");
                return null;
            }
            if (address.size() > 0) {
                Address location = address.get(0);
                location.getLatitude();
                location.getLongitude();
                p1 = new LatLng(location.getLatitude(), location.getLongitude());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return p1;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        // Setting onclick event listener for the map
        mMap.setOnMapClickListener((LatLng point) -> {
            if (clickGreen) {
                if (startMarker != null) startMarker.remove();
                startLocation = point;
                startMarkerOptions.position(startLocation);
                startMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                startMarker = mMap.addMarker(startMarkerOptions);
            } else if (clickRed) {
                if (destinationMarker != null) destinationMarker.remove();
                destinationLocation = point;
                destinationMarkerOptions.position(destinationLocation);
                destinationMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                destinationMarker = mMap.addMarker(destinationMarkerOptions);
            }
            executeDrawingRoute();
        });
    }

    private void executeDrawingRoute() {
        // Checks, whether etStart and end locations are captured
        if (startLocation != null && destinationLocation != null) {
            LatLng origin = startLocation;
            LatLng dest = destinationLocation;

            // Getting URL to the Google Directions API
            String url = getUrl(origin, dest);
            Log.d("onMapClick", url);
            FetchUrl fetchUrl = new FetchUrl();

            // Start downloading json data from Google Directions API
            fetchUrl.execute(url);
            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(origin));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        }
    }

    private String getUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream;
        HttpURLConnection urlConnection ;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();
            Log.d("downloadUrl", "Connected to URL");
            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();
            Log.d("downloadUrl", data);
            if (data.contains(OVER_QUERY_LIMIT))
                overLimitRequest = true;

            br.close();
            iStream.close();
            urlConnection.disconnect();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
            e.printStackTrace();
        }
        return data;
    }

    // Fetches data from url passed
    private class FetchUrl extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MapsActivity.this);
            progressDialog.setMessage(getString(R.string.searching_the_best_route));
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try {
                Log.d("fetchUrl", "Computing in background");
                // Fetching the data from web service
                data = downloadUrl(url[0]);
                Log.d("Background Task data", data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        private List<List<HashMap<String, String>>> routes = null;

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;

            try {
                jObject = new JSONObject(jsonData[0]);
                Log.d("ParserTask", jsonData[0]);
                DataParser parser = new DataParser();
                Log.d("ParserTask", parser.toString());
                // Starts parsing data
                routes = parser.parse(jObject);
                Log.d("ParserTask", routes.toString());
                Log.d("Distance", Float.toString(distance));
                setDistance(parser.getDistance());
            } catch (Exception e) {
                Log.d("ParserTask", e.toString());
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            if (routePolyline != null) routePolyline.remove();
            lineOptions = new PolylineOptions();
            if (routes != null && routes.size() != 0) {
                ArrayList<LatLng> points;
                if (result != null) {
                    // Traversing through all the routes
                    for (int i = 0; i < result.size(); i++) {
                        points = new ArrayList<>();

                        // Fetching i-th route
                        List<HashMap<String, String>> path = result.get(i);

                        // Fetching all the points in i-th route
                        for (int j = 0; j < path.size(); j++) {
                            HashMap<String, String> point = path.get(j);

                            double lat = Double.parseDouble(point.get("lat"));
                            double lng = Double.parseDouble(point.get("lng"));
                            LatLng position = new LatLng(lat, lng);
                            points.add(position);
                        }

                        // Adding all the points in the route to LineOptions
                        lineOptions.addAll(points);
                        lineOptions.width(10);
                        lineOptions.color(Color.RED);

                        Log.d("onPostExecute", "onPostExecute line options decoded");
                    }

                    // Drawing polyline in the Google Map for the i-th route
                    if (lineOptions != null) {
                        routePolyline = mMap.addPolyline(lineOptions);
                    } else {
                        Log.d("onPostExecute", "without Polylines drawn");
                    }
                    StringBuilder dialogStringDistance = new StringBuilder(getResources().getString(R.string.distance));
                    if (distanceUnits != null) {
                        if (distanceUnits.equals("miles")) {
                            String distanceMiles = String.format("%.2f", getDistance() * 0.621371192f / 1000);
                            dialogStringDistance.append(": ").append(distanceMiles).append(distanceUnits);
                            dialogShowDistance.setTitle(dialogStringDistance).show();
                        } else {
                            String distanceKm = String.format("%.2f", getDistance() / 1000);
                            dialogStringDistance.append(": ").append(distanceKm).append(distanceUnits);
                            dialogShowDistance.setTitle(dialogStringDistance).show();
                        }
                    } else {
                        String distanceKm = String.format("%.2f", getDistance() / 1000);
                        dialogStringDistance.append(": ").append(distanceKm).append(distanceUnits);
                        dialogShowDistance.setTitle(dialogStringDistance).show();
                    }
                }
            } else if (overLimitRequest) {
                Toast.makeText(MapsActivity.this, R.string.try_once_again, Toast.LENGTH_SHORT).show();
                overLimitRequest = false;
            } else {
                Toast.makeText(MapsActivity.this, R.string.there_is_no_connection, Toast.LENGTH_SHORT).show();
            }
            progressDialog.dismiss();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //etStop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can btnAdd here other case statements according to your requirement.
        }
    }

    private void setLocationFlags(boolean green, boolean red) {
        clickGreen = green;
        clickRed = red;
    }

    private void createDialogWithDistance() {
        dialogShowDistance = new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorYellow)
                .setIcon(R.drawable.ic_directions_white_24dp)
                .setTitle(R.string.distance + ": " + String.format("%.2f", getDistance()))
                .setMessage(R.string.do_you_want_to_add_to_calculator)
                .setPositiveButton(android.R.string.yes, v -> {
                    if (getDistance() != 0) {
                        Intent intent = new Intent(context, CalculatorActivity.class);
                        intent.putExtra("etDistance", getDistance());
                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.no, null);
    }

    private void initializeToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }
}