package com.trip.ben.calculator.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.adapter.MyAdapter;
import com.trip.ben.calculator.controller.DatabaseController;
import com.trip.ben.calculator.domain.Car;
import com.trip.ben.calculator.domain.Country;
import com.trip.ben.calculator.domain.Map;
import com.trip.ben.calculator.domain.Units;

import lombok.Getter;

public class FrontActivity extends NavigationDrawerActivity {
    private DatabaseController databaseController = new DatabaseController(this);
    private AdView mAdView;
    private RecyclerView recyclerView;
    private NavigationView navigationView;
    private Country country = Country.getInstance();
    private Map map = Map.getInstance();
    private Car car = Car.getInstance();
    private Units units = Units.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.activity_front, null, false);
        drawer.addView(contentView, 0);
        MobileAds.initialize(getApplicationContext(), "ca-app-pub-8691886908749926~8984605294");
        initializeCardTexts();
        initializeCurrencyCard();

        recyclerView = findViewById(R.id.front_data);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new MyAdapter(databaseController.getListOfFrontPageCards(), recyclerView, this));

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerView.setAdapter(new MyAdapter(databaseController.getListOfFrontPageCards(), recyclerView, this));
        for (int i = 0; i < navigationView.getMenu().size(); i++) {
            navigationView.getMenu().getItem(i).setChecked(false);
        }
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (id == R.id.action_home) {

        } else if (id == R.id.action_car) {
            startActivity(new Intent(this, SavedSettingsActivity.class));
            item.setChecked(false);
        } else if (id == R.id.action_maps) {
            startActivity(new Intent(this, MapsActivity.class));
            item.setChecked(false);
        } else if (id == R.id.action_calculator) {
            startActivity(new Intent(this, CalculatorActivity.class));
            item.setChecked(false);
        } else if (id == R.id.action_settings) {
            startActivity(new Intent(this, UserSettingsActivity.class));
            item.setChecked(false);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initializeCurrencyCard() {
        if (country.getDescription() == null) {
            country.setDescription(getResources().getString(R.string.not_chosen_currency_symbol));
        }
        if (country.getName() == null) {
            country.setName(getResources().getString(R.string.info_you_havent_chosen_currency));
        }
        if (country.getHeader() == null) {
            country.setHeader(getResources().getString(R.string.currency));
        }
    }

    public void initializeCardTexts() {
        if (car.getHeader() == null) {
            car.setHeader(getString(R.string.car));
        }
        if (map.getName() == null) {
            map.setName(getString(R.string.calculate_your_distance));
        }
        if (map.getDescription() == null) {
            map.setDescription(getString(R.string.enter_to_choose_route));
        }
        if (map.getHeader() == null) {
            map.setHeader(getString(R.string.map));
        }
        if (units.getHeader() == null) {
            units.setHeader(getString(R.string.units));
        }
    }
}
