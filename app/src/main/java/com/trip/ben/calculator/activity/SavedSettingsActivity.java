package com.trip.ben.calculator.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.trip.ben.calculator.R;
import com.trip.ben.calculator.adapter.SettingsAdapter;
import com.trip.ben.calculator.database.UserDatabase;
import com.trip.ben.calculator.domain.Settings;

import java.util.ArrayList;
import java.util.List;

import lombok.Setter;
import solid.stream.Stream;

public class SavedSettingsActivity extends AppCompatActivity {

    public static Activity activity;
    private AdView mAdView;
    private UserDatabase userDatabase;
    private List<Settings> list;
    @Setter
    private String clickedPosition = "";
    private List<Integer> listOfSettings;
    private RecyclerView recyclerView;
    private Context context;
    private FloatingActionButton btnAdd;
    private FloatingActionButton btnEdit;
    private FloatingActionButton btnDelete;
    private FloatingActionButton btnSave;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_settings);
        activity = this;
        
        userDatabase = new UserDatabase(this);
        list = userDatabase.getAll();
        listOfSettings = new ArrayList<>();

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        initializeToolbar();

        btnAdd = findViewById(R.id.fab_add);
        btnAdd.setOnClickListener(v -> {
            startActivity(new Intent(this, UserSettingsActivity.class));
        });

        btnEdit = findViewById(R.id.fab_edit);
        btnEdit.setOnClickListener(v -> {
            if (!clickedPosition.equals("")) {
                context = getApplicationContext();
                Intent intent = new Intent(context, UpdateSettingsActivity.class);
                intent.putExtra("car_number", listOfSettings.get(Integer.parseInt(clickedPosition)));
                startActivity(intent);
            } else {
                Toast.makeText(SavedSettingsActivity.this, R.string.you_didnt_select_car, Toast.LENGTH_SHORT).show();
            }
        });

        btnDelete = findViewById(R.id.fab_delete);
        btnDelete.setOnClickListener(v -> {
            if (!clickedPosition.equals("")) {
                userDatabase.deleteUserSettings(listOfSettings.get(Integer.parseInt(clickedPosition)));
                listOfSettings.remove(Integer.parseInt(clickedPosition));
                Toast.makeText(SavedSettingsActivity.this, R.string.delete_completed, Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            } else {
                Toast.makeText(SavedSettingsActivity.this, R.string.you_didnt_select_car, Toast.LENGTH_SHORT).show();
            }
        });

        btnSave = findViewById(R.id.fab_save);
        btnSave.setOnClickListener(v -> {
            if (!clickedPosition.equals("")) {
                for (Settings settings : list) {
                    if (settings.getNr() == listOfSettings.get(Integer.parseInt(clickedPosition))) {
                        settings.setChecked(true);
                    } else {
                        settings.setChecked(false);
                    }
                    userDatabase.updateUserSettings(settings);
                }
                Toast.makeText(SavedSettingsActivity.this, R.string.toast_settings_saved, Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
            }
        });

        for (Settings settings : list) {
            listOfSettings.add(settings.getNr());
        }

        Settings settingsOfChosenCarAsDefault = Stream.stream(list)
                .filter(Settings::isChecked)
                .first()
                .orNull();
        int idOfChosenCarAsDefault = (settingsOfChosenCarAsDefault != null)
                ? settingsOfChosenCarAsDefault.getNr() : -1;

        for (int i = 0; i < listOfSettings.size(); i++) {
            if (idOfChosenCarAsDefault > -1 && listOfSettings.get(i) == idOfChosenCarAsDefault) {
                clickedPosition = Integer.toString(i);
            }
        }


        recyclerView = findViewById(R.id.saved_data);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new SettingsAdapter(recyclerView, this, list, listOfSettings));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerView.setAdapter(new SettingsAdapter(recyclerView, this, list, listOfSettings));

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    private void initializeToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }
}
