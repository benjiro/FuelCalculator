package com.trip.ben.calculator.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trip.ben.calculator.R;
import com.trip.ben.calculator.activity.SavedSettingsActivity;
import com.trip.ben.calculator.domain.Settings;

import java.util.List;

/**
 * Created by Benjamin on 2017-11-05.
 */

public class SettingsAdapter extends RecyclerView.Adapter {
    private int clickedPosition = -1;
    private RecyclerView mRecyclerView;
    private Context context;
    private List<Settings> mListOfSettings;
    private List<Integer> mListOfIds;

    public SettingsAdapter(RecyclerView pRecyclerView, Context context, List list, List listOfIds) {
        mRecyclerView = pRecyclerView;
        mListOfSettings = list;
        mListOfIds = listOfIds;
        this.context = context;
    }

    private class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mContent;
        public TextView mNumber;
        public CardView mCardView;

        public MyViewHolder(View pItem) {
            super(pItem);
            mNumber = pItem.findViewById(R.id.settings_nr);
            mTitle = pItem.findViewById(R.id.settings_name);
            mContent = pItem.findViewById(R.id.settings_info);
            mCardView = pItem.findViewById(R.id.cv_settings);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.settings_layout, viewGroup, false);

        view.setOnClickListener(v -> {
            clickedPosition = mRecyclerView.getChildAdapterPosition(v);
            notifyDataSetChanged();
            ((SavedSettingsActivity) context).setClickedPosition(Integer.toString(clickedPosition));
            Log.d("onCreateViewHolder", Integer.toString(clickedPosition));
        });

        return new SettingsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int i) {
        Settings settings = mListOfSettings.get(i);
        if (settings.isChecked()) {
            ((MyViewHolder) viewHolder).mCardView.setBackgroundColor(0xFFcf5a5a);
        } else if (clickedPosition == i) {
            ((MyViewHolder) viewHolder).mCardView.setBackgroundColor(0xFFe4c863);
        } else {
            ((MyViewHolder) viewHolder).mCardView.setBackgroundColor(Color.WHITE);
        }
        ((SettingsAdapter.MyViewHolder) viewHolder).mTitle.setText(settings.getCarName());
        ((SettingsAdapter.MyViewHolder) viewHolder).mContent
                .setText(settings.getConsumption().toString() + settings.getUnitsConsumption());
        ((SettingsAdapter.MyViewHolder) viewHolder).mNumber.setText(Integer.toString(i + 1));
    }

    @Override
    public int getItemCount() {
        return mListOfSettings.size();
    }
}
