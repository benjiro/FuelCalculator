package com.trip.ben.calculator.domain;

/**
 * Created by Benjamin on 2017-11-02.
 */

public abstract class FrontPageCard {
    public abstract String getName();
    public abstract String getDescription();
    public abstract String getHeader();
    public abstract int getIconId();
    public abstract void setName(String name);
    public abstract void setDescription(String description);
    public abstract void setHeader(String header);
    public abstract void setIconId(int id);
}
